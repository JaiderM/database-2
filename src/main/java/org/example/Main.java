package org.example;

import java.sql.Date;
import java.time.LocalDate;

public class Main {
  public static void main(String[] args) {

    try {
      //Conexion a la base de datos
      DataBaseOperation da = new DataBaseOperation();
      //Agragamos un estudiante
      da.createStudents("Andres","jimenez", Date.valueOf(LocalDate.now()),"masculino","calle 26","8383394");
      //Leemos Estudiantes
      da.readStudents();
      //Leemos Notas
      da.readNote();
      //Actualizar Notas
      da.updateNota(1.1f);
      //Eliminar Estudiante
      da.deleteStudent(8);

    } catch (Exception ex){
      System.out.println(ex.getMessage());
    }
  }

}