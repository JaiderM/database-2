package org.example;

import java.sql.*;

public class DataBaseOperation {
  private Statement statement;
  private Connection connection;

  /**
   * Constructor de la clase DataBaseOperation
   * Crea una conexión a la base de datos SQL Server
   */
  public DataBaseOperation() {

    try {
      System.out.println("Create connection.");
      Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
      connection = DriverManager.getConnection("jdbc:sqlserver://localhost;databaseName=Schools;encrypt=false;trustServerCertificate=true", "sa", "Jaider");
      this.statement = connection.createStatement();
    } catch (Exception ex) {
      System.out.println(ex.getMessage());
    }
  }

  /**
   * Inserta un nuevo estudiante llamando al procedimiento almacenado insertarEstudiante.
   *
   * @param name Nombre del estudiante
   * @param lastName Apellido del estudiante
   * @param briday Fecha de nacimiento del estudiante
   * @param gender Género del estudiante
   * @param address Dirección del estudiante
   * @param phoneNumber Teléfono del estudiante
   * @throws SQLException Si ocurre un error llamando al procedimiento
   */
  public void createStudents(String name, String lastName, Date briday, String gender, String address, String phoneNumber) throws SQLException {
    System.out.println("\nCreating data...");
    CallableStatement cs = this.connection.prepareCall("{call insertarEstudiante(?,?,?,?,?,?)}");

    cs.setString(1, name);
    cs.setString(2, lastName);
    cs.setDate(3, briday);
    cs.setString(4, gender);
    cs.setString(5, address);
    cs.setString(6, phoneNumber);

    cs.executeUpdate();
  }

  /**
   * Consulta y muestra los estudiantes desde la vista VistaEstudiantes.
   *
   * @throws SQLException Si ocurre un error al consultar la BD.
   */
  public void readStudents() throws SQLException {
      System.out.println("\nReading data...");
      ResultSet resultSet = this.statement.executeQuery("SELECT * FROM VistaEstudiantes;");

      while (resultSet.next()) {
        System.out.println(resultSet.getInt("ID") + " : " + resultSet.getString("Nombre") + " - "
                + resultSet.getString("Apellido") + " - " + resultSet.getDate("FechaNacimiento"));
      }
  }

  /**
   * Consulta y muestra las notas de los estudiantes
   * desde la vista VistaNotas.
   *
   * @throws SQLException si ocurre un error al consultar la BD.
   */
  public void readNote() throws SQLException {
    System.out.println("\nReading data...");
    ResultSet resultSet = this.statement.executeQuery("SELECT * FROM VistaNotas;");

    while (resultSet.next()) {
      System.out.println(resultSet.getString("Estudiante") + " : " + resultSet.getString("Materia") + " - "
              + resultSet.getString("Profesor") + " - " + resultSet.getFloat("Nota"));
    }
  }

  /**
   * Actualiza las notas de los estudiantes llamando
   * al procedimiento almacenado ActualizarNotas.
   *
   * @param nota El porcentaje en que se actualizarán las notas.
   * @throws SQLException Si ocurre un error llamando al procedimiento.
   */
  public void updateNota(float nota) throws SQLException {
    CallableStatement cs = this.connection.prepareCall("{call ActualizarNotas(?)}");
    cs.setFloat(1, nota);
    cs.executeUpdate();
  }

  /**
   * Elimina un estudiante por su ID llamando al procedimiento
   * almacenado eliminarEstudiante.
   *
   * @param id El ID del estudiante a eliminar.
   * @throws SQLException Si ocurre un error llamando al procedimiento.
   */
  public void deleteStudent(int id) throws SQLException {
    CallableStatement cs = this.connection.prepareCall("{call eliminarEstudiante(?)}");
    cs.setFloat(1, id);
    cs.executeUpdate();
  }
}
