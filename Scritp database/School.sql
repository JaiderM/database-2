
-- CREAR BASE DE DATOS

CREATE DATABASE Schools;

USE Schools;

CREATE TABLE Estudiante (
ID INT IDENTITY(1,1) PRIMARY KEY, 
Nombre VARCHAR(50),
Apellido VARCHAR(50),
FechaNacimiento DATE,
Genero VARCHAR(10),
Direccion VARCHAR(100),  
Telefono VARCHAR(15)
);

CREATE TABLE Profesor (
ID INT IDENTITY(1,1) PRIMARY KEY,
Nombre VARCHAR(50),
Apellido VARCHAR(50),
Especialidad VARCHAR(50),
Telefono VARCHAR(15),
CorreoElectronico VARCHAR(100)
);

CREATE TABLE Materia (
ID INT IDENTITY(1,1) PRIMARY KEY,
NombreMateria VARCHAR(100), 
Descripcion VARCHAR(200)
);

CREATE TABLE Inscripciones (
IDEstudiante INT,
IDMateria INT,
PRIMARY KEY (IDEstudiante, IDMateria),
FOREIGN KEY (IDEstudiante) REFERENCES Estudiante(ID),
FOREIGN KEY (IDMateria) REFERENCES Materia(ID)  
);

CREATE TABLE Notas (
ID INT IDENTITY(1,1) PRIMARY KEY,
IDEstudiante INT,
IDMateria INT,  
IDProfesor INT,
Calificacion FLOAT,
FOREIGN KEY (IDEstudiante) REFERENCES Estudiante(ID),
FOREIGN KEY (IDMateria) REFERENCES Materia(ID),
FOREIGN KEY (IDProfesor) REFERENCES Profesor(ID)
);

-- INSERTAR DATOS EN LA BASE DE DATOS

INSERT INTO Estudiante (Nombre, Apellido, FechaNacimiento, Genero, Direccion, Telefono)
VALUES 
('Ana', 'Martinez', '1998-11-02', 'Femenino', 'Calle 3', '5554561212'),
('Pablo', 'Rodriguez', '1997-03-15', 'Masculino', 'Calle 4', '5551236547'),
('Lucia', 'Lopez', '2001-08-30', 'Femenino', 'Calle 5', '5553216549'),
('Pedro', 'Garcia', '2002-06-22', 'Masculino', 'Calle 6', '5557896541'),
('Sofia', 'Fernandez', '2000-12-31', 'Femenino', 'Calle 7', '5556541238'),
('Juan', 'Perez', '2000-01-01', 'Masculino', 'Calle 1', '5551231234'),
('Maria', 'Gonzalez', '1999-05-05', 'Femenino', 'Calle 2', '5553213212');

INSERT INTO Profesor (Nombre, Apellido, Especialidad, Telefono, CorreoElectronico)
VALUES
('Jorge', 'Ruiz', 'F�sica', '5559874561', 'jorge@email.com'),
('M�nica', 'Cruz', 'Qu�mica', '5556547892', 'monica@email.com'),  
('Alberto', 'Guti�rrez', 'Biolog�a', '5551236598', 'alberto@email.com'),
('Isabel', 'Jim�nez', 'Historia', '5554563218', 'isabel@email.com'),
('Daniel', 'Mart�nez', 'Geograf�a', '5557894561', 'daniel@email.com'),
('Carlos', 'Gomez', 'Matematicas', '5551231212', 'carlos@email.com'),
('Luisa', 'Sanchez', 'Literatura', '5532121245', 'luisa@email.com');

INSERT INTO Materia (NombreMateria, Descripcion)  
VALUES
('F�sica', 'Materia sobre f�sica y mec�nica'),
('Qu�mica', 'Materia sobre elementos y reacciones'),
('Biolog�a', 'Materia sobre seres vivos'), 
('Historia', 'Materia sobre civilizaciones'),
('Geograf�a', 'Materia sobre lugares y regiones'),
('Matematicas', 'Materia sobre numeros'),
('Literatura', 'Materia sobre libros y autores');

INSERT INTO Inscripciones (IDEstudiante, IDMateria)
VALUES 
(3, 3),
(4, 4),
(5, 5),
(6, 1), 
(7, 2),
(1, 1),
(2, 2);
  
INSERT INTO Notas (IDEstudiante, IDMateria, IDProfesor, Calificacion)
VALUES
(3, 3, 3, 7.8),
(4, 4, 4, 9.2),
(5, 5, 5, 8.5),
(6, 1, 1, 6.7),
(7, 2, 2, 9.8),
(1, 1, 1, 9.5),
(2, 2, 2, 8.7);

       
-- INICIA EL LABORATORIO 

-- VISTAS

-- Vista simple 
CREATE VIEW VistaEstudiantes AS
SELECT ID, Nombre, Apellido 
FROM Estudiante

-- ver resultado
SELECT * FROM VistaEstudiantes;


-- Vista con JOIN
CREATE VIEW VistaNotasEstudiantes AS
SELECT e.Nombre, e.Apellido, m.NombreMateria, n.Calificacion
FROM Notas n
INNER JOIN Estudiante e ON n.IDEstudiante = e.ID
INNER JOIN Materia m ON n.IDMateria = m.ID

-- ver resultado
SELECT * FROM VistaNotasEstudiantes;

-- VISTA PARA VER LAS NOTAS -- PARA EL CRUD
CREATE VIEW VistaNotas AS
SELECT 
  e.Nombre AS Estudiante,
  m.NombreMateria As Materia,
  p.Nombre AS Profesor,
  n.Calificacion AS Nota
FROM Notas n
INNER JOIN Estudiante e ON n.IDEstudiante = e.ID
INNER JOIN Materia m ON n.IDMateria = m.ID  
INNER JOIN Profesor p ON n.IDProfesor = p.ID

SELECT * FROM VistaNotas;

-- VISTAS CON CONDICION WHERE

CREATE VIEW VistaAprobados AS  
SELECT * FROM VistaNotasEstudiantes
WHERE Calificacion >= 70

-- ver resultado
SELECT * FROM VistaAprobados;


-- CREACION DE VISTAS INDEXADAS

CREATE VIEW VistaEstudianteIndexada  
WITH SCHEMABINDING  
AS
SELECT ID, Nombre, Apellido   
FROM dbo.Estudiante

CREATE UNIQUE CLUSTERED INDEX IX_VistaEstudiantes ON VistaEstudianteIndexada(ID);

-- ver resultado
SELECT * FROM VistaEstudianteIndexada;


-- MODIFICACION DE VISTAS

ALTER VIEW VistaEstudiantes AS
SELECT ID, Nombre, Apellido, FechaNacimiento
FROM Estudiante

-- ver resultado
SELECT * FROM VistaEstudiantes;


-- ACTUALIZACION DE TABLAS A TRAVES DE VISTAS

UPDATE VistaEstudiantes 
SET Nombre = 'Juan'
WHERE ID = 1

-- ver resultado
SELECT * FROM VistaEstudiantes;

SELECT *
FROM sys.views
WHERE type = 'V'

-- ELIMINAR UNA VISTA
DROP VIEW VistaEstudianteIndexada;
       

-- PROCEDIMIENTOS DE ALMACENADO --
     

-- MOSTRAR DATOS CON PARAMETROS

CREATE PROCEDURE spMostrarEstudiantes  
@Genero VARCHAR(10)
AS
BEGIN
   SELECT * FROM Estudiante
   WHERE Genero = @Genero
END

-- ver resultado
EXEC spMostrarEstudiantes @Genero = 'Masculino'


-- INSERTAR DATOS CON CONDICIONAL

CREATE PROCEDURE spInsertarNota
@IDEstudiante INT,
@IDMateria INT, 
@IDProfesor INT,
@Nota INT
AS
BEGIN
  IF @Nota > 100 OR @Nota < 0 
    BEGIN
       PRINT 'Nota inv�lida'
       RETURN
    END
  
  INSERT INTO Notas (IDEstudiante, IDMateria, IDProfesor, Calificacion)
  VALUES (@IDEstudiante, @IDMateria, @IDProfesor, @Nota)
END

-- ver resultado
EXEC spInsertarNota @IDEstudiante = 1, @IDMateria = 2, @IDProfesor = 3, @Nota = 65

SELECT * FROM Notas;

-- ACTUALIZAR CON BUCLE

CREATE PROCEDURE spActualizarNotas
AS
BEGIN
  DECLARE @IDNota INT
  
  DECLARE NotasCursor CURSOR FOR 
  SELECT ID FROM Notas
  
  OPEN NotasCursor 
  
  FETCH NEXT FROM NotasCursor INTO @IDNota
  
  WHILE @@FETCH_STATUS = 0
  BEGIN
     UPDATE Notas 
     SET Calificacion = Calificacion * 1.1
     WHERE ID = @IDNota
     
     FETCH NEXT FROM NotasCursor INTO @IDNota
  END
  
  CLOSE NotasCursor
  DEALLOCATE NotasCursor
END

-- ver resultado
EXEC spActualizarNotas

-- ACTULIZAR NOTAS CON PARAMETRO DE LA CANTIDAD QUE SE DESEA SUBIR
CREATE PROCEDURE ActualizarNotas
@cantidad FLOAT	
AS
BEGIN
  DECLARE @IDNota INT
  
  DECLARE NotasCursor CURSOR FOR 
  SELECT ID FROM Notas
  
  OPEN NotasCursor 
  
  FETCH NEXT FROM NotasCursor INTO @IDNota
  
  WHILE @@FETCH_STATUS = 0
  BEGIN
     UPDATE Notas 
     SET Calificacion = Calificacion * @cantidad
     WHERE ID = @IDNota
     
     FETCH NEXT FROM NotasCursor INTO @IDNota
  END
  
  CLOSE NotasCursor
  DEALLOCATE NotasCursor
END


-- MANEJO DE EXEPCIONES

CREATE PROCEDURE spInsertarEstudiante
@Nombre VARCHAR(50),
@Apellido VARCHAR(50) 
AS
BEGIN TRY

    IF @Nombre IS NULL OR LEN(@Nombre) = 0
    RAISERROR('El nombre no puede ser nulo', 16, 1)

    IF @Apellido IS NULL OR LEN(@Apellido) = 0
    RAISERROR('El apellido no puede ser nulo', 16, 1)

  INSERT INTO Estudiante (Nombre, Apellido)
  VALUES (@Nombre, @Apellido)
END TRY

BEGIN CATCH
  PRINT 'Error insertando estudiante'
END CATCH

-- ver resultado
EXEC spInsertarEstudiante @Nombre='', @Apellido='Garcia'

-- ELIMINAR UN ESTUDIANTE POR ID

CREATE PROCEDURE eliminarEstudiante
@id INT
AS
BEGIN TRY
  DELETE FROM Estudiante 
  WHERE ID = @id
END TRY
BEGIN CATCH
  PRINT 'Error insertando estudiante'
END CATCH

-- INSERTAR UN ESTUDIANTE CON TODOS SUS DATOS

CREATE PROCEDURE insertarEstudiante
@Nombre VARCHAR(50),
@Apellido VARCHAR(50),
@FechaNacimiento DATE,
@Genero VARCHAR(10), 
@Direccion VARCHAR(100),
@Telefono VARCHAR(15)
AS
BEGIN TRY

  INSERT INTO Estudiante (Nombre, Apellido, FechaNacimiento, Genero, Direccion, Telefono)
  VALUES(@Nombre, @Apellido, @FechaNacimiento, @Genero, @Direccion, @Telefono)

END TRY
BEGIN CATCH

  DECLARE @errorMsg nvarchar(500) = ERROR_MESSAGE()
  RAISERROR(@errorMsg, 16, 1)

END CATCH


-- MIGRAR DATOS CON PROCEDIMIENTOS

-- TABLA PARA ESTE PROCESO

CREATE TABLE NotasHistorico (
  ID int, 
  IDEstudiante int,
  IDMateria int,
  IDProfesor int,
  Calificacion float
)

-- VISTA IMPLEMENTADA

CREATE VIEW NotasOLD AS
SELECT * FROM NotasHistorico;

-- PROCEDIMIENTO

CREATE PROCEDURE spMigrarNotas
AS
BEGIN
  INSERT INTO NotasOLD
    SELECT * FROM Notas
    
  DELETE FROM Notas
END

-- ver resultado
EXEC spMigrarNotas

SELECT * FROM NotasOLD;

SELECT * FROM Notas;

-- TRIGGERS 

-- TABLA PARA ESTE PROCESO

CREATE TABLE BitacoraNotas (
  Descripcion VARCHAR(500),
  Fecha DATETIME,
  Usuario VARCHAR(100)
)

-- REGISTRO DE CAMBIOS EN TABLAS

CREATE TRIGGER tr_RegistrarCambios ON Notas 
AFTER INSERT, UPDATE, DELETE
AS
BEGIN
  INSERT INTO BitacoraNotas
  VALUES (
    'Cambio realizado en Notas',
    GETDATE(),
    CURRENT_USER
  )
END

-- PRUEBA: INSERTAMOS NOTAS
INSERT INTO Notas (IDEstudiante, IDMateria, IDProfesor, Calificacion)
VALUES
(3, 3, 3, 7.8);

-- VER RESULTADO DEL TRIGGRER

SELECT * FROM BitacoraNotas;


-- VALIDACION ANTES DE PERMITIR OPERACIONES 
CREATE TRIGGER tr_ValidarNotas ON Notas 
INSTEAD OF INSERT
AS
BEGIN
  IF (SELECT COUNT(*) FROM inserted WHERE Calificacion < 0 OR Calificacion > 100) > 0
  BEGIN
     SELECT 'No se permiten notas fuera del rango 0-100' AS Mensaje
     RETURN
  END
  
 INSERT INTO Notas (IDMateria, IDEstudiante, Calificacion)
  SELECT IDMateria, IDEstudiante, Calificacion 
  FROM inserted
END

-- PRUEBA DE INSERTAR NOTAS
INSERT INTO Notas (IDEstudiante, IDMateria, IDProfesor, Calificacion)
VALUES
(3, 4, 5, 120);


-- REGISTRAR HISTORIAL

-- TABLA PARA ESTE PROCESO
CREATE TABLE NotasHistorial (
  ID int, 
  IDEstudiante int,
  IDMateria int, 
  IDProfesor int,
  Nota decimal(5,2),
  Operacion varchar(20) 
);

-- PROCEDIMIENTO

CREATE TRIGGER tr_HistorialNotas ON Notas
AFTER UPDATE
AS  
BEGIN
  INSERT INTO NotasHistorial
  SELECT *, 'Actualizado' FROM deleted
  
  INSERT INTO NotasHistorial
  SELECT *, 'Nuevo' FROM inserted 
END

-- PRUEBA DE ACTUALIZAR NOTAS
UPDATE Notas
set IDProfesor = 4
where Calificacion = 100

SELECT * FROM NotasHistorial;


